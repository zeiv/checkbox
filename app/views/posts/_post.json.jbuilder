json.extract! post, :id, :title, :investment_types, :created_at, :updated_at
json.url post_url(post, format: :json)